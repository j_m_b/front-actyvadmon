import React, {useContext, useEffect} from 'react';
import Activity from './Activity';
import ActivityContext from '../../context/activities/activityContext';
import { CSSTransition, TransitionGroup } from 'react-transition-group';


// materiaul-ui
import {
    List,
} from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';



const useStyles = makeStyles((theme) => ({
    root: {
        padding: '1rem',
    },
}));

const ListOfActivities = () => {

    // extraer de activity context
    const activityContext = useContext(ActivityContext);
    const { activities, message, activitiesList } = activityContext;

    // obtener lista de actividades al cargar componente
    useEffect(() => {
        // si hay errores 
        if (message) {
            alert("Hubo un error");
        }

        activitiesList();
        // eslint-disable-next-line
    }, [message]);


    // meterial-ui
    const classes = useStyles();

    // validar si hay proyectos
    if (activities.length === 0) return <Typography className={classes.root} component="span" variant="body2">No hay actividades</Typography>;


    return <>
        <List>
            <Typography
                component="span"
                variant="body2"
                className={classes.root}>
                Lista de actividades
            </Typography>
            <TransitionGroup>
                { activities.map(activity => (
                    <CSSTransition
                        key={activity._id}
                        timeout={300}
                    >
                        <Activity 
                            activity={activity}
                        />
                    </CSSTransition>
                ))}
            </TransitionGroup>
        </List>
    </>
}

export default ListOfActivities;
import React, {useContext} from 'react';
import SideBar from '../layout/SideBar';
import Nav from './Nav';
import TaskPanel from '../tasks/TaskPanel';
// import AuthContext from '../../context/auth/authContext';
import ActivityContext from '../../context/activities/activityContext';



import { makeStyles } from '@material-ui/core/styles';



const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));



const Activities = () => {

    // extraer de authcontext
    // const authContext = useContext(AuthContext);
    // const { user, userAuthenticate, logOut } = authContext;

    // extraer de activity context
    const activityContext = useContext(ActivityContext);
    const { activity } = activityContext;


    // Material-UI //
    const classes = useStyles();

    return <>
        <div className={classes.root}>

            <SideBar />
            <div className="main-section">

                <main className={classes.content}>
                    { activity ? ( 
                        <div>
                            <Nav />
                            <TaskPanel />
                            <div className={classes.toolbar} />
                        </div>
                    ) : (
                        <h3>Selecciona una actividad</h3>
                    )}
                </main>

            </div>
        </div>

    </>
}

export default Activities;
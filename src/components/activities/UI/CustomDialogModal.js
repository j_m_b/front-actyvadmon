import React, {useContext, useState} from 'react';
import ActivityContext from '../../../context/activities/activityContext';


import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


const CustomDialogModal = ({
    open,
    setOpen,
    handleClose,
    action,
    setAction
}) => {
    // extraer de activity context
    const activityContext = useContext(ActivityContext);
    const {activity, updateActivity, removeActivity } = activityContext;
    
    // state local para actualizar actividad
    let activitySelected = null;
    activitySelected = activity;
    const [ activityUpdated, setActivityUpdate] = useState({
        name: activitySelected.name,
    })
    const handleChange = (e) => {
        setActivityUpdate({
            ...activityUpdated,
            [e.target.name]: e.target.value
        })
    }
    let { name } = activityUpdated;

    // state local para agregar usuario a actividad
    const [ newUser, setNewUser] = useState({
        email: '',
    })
    const handleChangeUser = (e) => {
        setNewUser({
            ...newUser,
            [e.target.name]: e.target.value
        })
    }
    const { email } = newUser;

    // onsubmit para actulizar actividad
    const onsubmitActivity = (e) => {
        e.preventDefault();

        // si la accion es actualizar la actividad
        if (action === 'update') {
            // validar campos vacios
            if (name.trim() === '') {
                alert('No pueden haber campos vacios');
                return;
            };

            // editar actividad
            activitySelected.name = name;
            updateActivity(activitySelected);
        }

        // si la accion es agregar nuevo usuarui
        if (action === 'add_user') {
            // validar campos vacios
            if (email.trim() === '') {
                alert('No pueden haber campos vacios');
                return;
            };

            // agregar usuario
            activitySelected.email = email;
            updateActivity(activitySelected);
    }

        // reiniciar states
        setOpen(false);
        setAction('');
    }

    // eliminar actividad
    const deleteActivity = () =>{
        removeActivity(activitySelected._id);
        
        // reiniciar states
        setOpen(false);
        setAction('');
    }

    // dependiendo de la accion que el usuario desee hacer el titulo, texto y button cambian
    let title = '';
    let text = '';
    let form = '';
    let actionButton = '';

    // eliminar actividad
    if (action === 'delete') {
        title = '¿Eliminar actividad?';
        text = 'Esta actividad se eliminara permanentemente incluyendo sus tareas. Tambien perderas la lista de miembros';
        actionButton = (
            <Button onClick={deleteActivity} color="primary">
                Eliminar
            </Button>
        );
    }

    // editar actividad
    if (action === 'update') {
        title = '¿Editar actividad?';
        text = 'Modifique los valores que desea editar en este formulario';
        form = (
            <TextField 
                label="Nombre actividad"
                name="name"
                value={name}
                onChange={handleChange}
                variant="filled" 
                fullWidth
            />
        );
        actionButton = (
            <Button type="submit" color="primary">
                Guardar
            </Button>
        );
    }

    // nuevo miembro en la actividad
    if (action === 'add_user') {
        title = 'Compartir actividad con usuario';
        text = 'Debe ingresar el email del usuario que desea agregar';
        form = (
            <TextField 
                type="email"
                label="Email"
                name="email"
                value={email}
                onChange={handleChangeUser}
                variant="filled" 
                fullWidth
            />
        );
        actionButton = (
            <Button type="submit" color="primary">
                Guardar
            </Button>
        );
    }

    const DialogModal = (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{`${title}`}</DialogTitle>
            <form 
                onSubmit={onsubmitActivity}                           
            >
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {text}
                        { form ? form : null }
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" autoFocus>
                        Cancelar
                    </Button>
                    {actionButton}
                </DialogActions>
            </form>
        </Dialog>
    );

    return DialogModal;

}

export default CustomDialogModal;
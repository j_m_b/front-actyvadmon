import React, {useContext} from 'react';
// import PropTypes from 'prop-types';
import ActivityContext from '../../context/activities/activityContext';
import TaskContext from '../../context/tasks/taskContext';
import AuthContext from '../../context/auth/authContext';

// material-ui
// materiaul-ui
import {
    ListItem,
    ListItemIcon,
    ListItemText,
} from '@material-ui/core/';
import AccessibilityIcon from '@material-ui/icons/Accessibility';
import GroupWorkIcon from '@material-ui/icons/GroupWork';

const Activity = ({activity}) => {

    // extraer de activity context
    const activityContext = useContext(ActivityContext);
    const { currentActivity, memberList } = activityContext;

    // extraer de task context
    const taskContext = useContext(TaskContext);
    const { taskList } = taskContext;

    // extraer de auth context
    const authContext = useContext(AuthContext);
    const { user } = authContext;

    // useEffect(() => {
    //     userAuthenticate();
    // }, []);

    const selectActivity = (activity) => {
        currentActivity(activity._id);
        taskList(activity._id);
        memberList(activity.members);
    }


    return <>
        { activity && user ? 
        <ListItem 
            button 
            key={activity._id}
            onClick={() => selectActivity(activity)}
        >
            <ListItemIcon>
                {user._id === activity.owner ? <AccessibilityIcon /> : <GroupWorkIcon /> }
            </ListItemIcon>
            <ListItemText primary={activity.name} />
        </ListItem>
        : null}
    </>
}

export default Activity;


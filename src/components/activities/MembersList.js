import React, { useContext } from 'react';
import ActivityContext from '../../context/activities/activityContext';
import AuthContext from '../../context/auth/authContext';

// material-ui
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 'relative',
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));


const MemberList = () => {
    // extraer de activity context
    const activityContext = useContext(ActivityContext);
    const { activity, members, updateActivity, removeMember } = activityContext;

    // extraer de auth context
    const authContext = useContext(AuthContext);
    const { user } = authContext;

    // eliminar miembro
    const deleteMember = (member) => {
        const currentActivity = activity;

        currentActivity.email = member.email;
        currentActivity.actionDelete = true;

        updateActivity(currentActivity);
        removeMember(member);
    }

    // material-ui
    const classes = useStyles();

    if (members.length === 0) return <p>No hay miembros en esta actividad</p>

    return <>
        <Typography
            component="span"
            variant="body2"
            className={classes.inline}
            color="textPrimary"
        >
            Lista de miembros
        </Typography>
        <List className={classes.root}>
            {members.map(member => (
                <>
                    <ListItem alignItems="flex-start">
                        <ListItemAvatar>
                            <Avatar>{member.userName[0]}</Avatar>
                        </ListItemAvatar>
                        <ListItemText
                            primary={member.userName}
                            secondary={
                                <React.Fragment>
                                    <Typography
                                        component="span"
                                        variant="body2"
                                        className={classes.inline}
                                        color="textPrimary"
                                    >
                                        {member.email}
                                    </Typography>
                                    <>
                                        {user._id.toString() === activity.owner.toString() ? (
                                            <Typography
                                            >
                                                <Link href="#" color="secondary" onClick={() => deleteMember(member)}>
                                                    Eliminar
                                                </Link>
                                            </Typography>

                                        ) : null}
                                    </>
                                </React.Fragment>
                            }
                        />
                    </ListItem>
                    <Divider variant="inset" component="li" />
                </>
            ))}
        </List>
    </>
}

export default MemberList;
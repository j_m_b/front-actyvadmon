import React, { useContext, useState } from 'react';
import ActivityContext from '../../context/activities/activityContext';

// material ui
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(2),
            width: '25ch',
        },
    },
}));


const NewActivity = () => {

    // extraer de activity context
    const activityContext = useContext(ActivityContext);
    const {
        activityForm,
        // activityError,
        showForm,
        hideForm,
        // showError,
        newActivity
    } = activityContext;

    // states locales para nueva actividad
    const [activity, setActivity] = useState({
        name: '',
    });
    const { name } = activity;

    // leer cambios del state
    const onchangeActivity = (e) => {
        setActivity({
            ...activity,
            [e.target.name]: e.target.value,
        })
    }

    // agregar nueva actividad
    const onsubmitActivity = (e) => {
        e.preventDefault();

        // validar campos vacios
        if (name.trim() === '') {
            alert('Error: campos vacios');
            // showError()
            return;
        }

        // pasar state local a state global
        newActivity(activity);

        // reiniciar formulario
        setActivity({
            name: '',
        })
    }


    // material-ui
    const classes = useStyles();


    return <>
        <div className={classes.root}>
            {activityForm ? (
                <Button
                type="button"
                onClick={() => hideForm()}
                >Cancelar</Button>
            ) : (
                <Button
                type="button"
                onClick={() => showForm()}
                >Nueva actividad</Button>
            )}


            {activityForm
                ? (
                    <form
                        onSubmit={onsubmitActivity}
                    >
                        <TextField
                            label="Nueva actividad" 
                            variant="outlined" 
                            type="text"
                            name="name"
                            onChange={onchangeActivity}
                            value={name}
                        />
                        <Button
                            color="primary"
                            variant="contained"
                            fullWidth
                            type="submit"
                        >Guardar</Button>
                    </form>
                ) : null
            }
        </div>
    </>
}

export default NewActivity;
import React, { useContext } from 'react';
import ActivityContext from '../../context/activities/activityContext';
import AuthContext from '../../context/auth/authContext';
import TaskContext from '../../context/tasks/taskContext';

import CustomDialogModal from './UI/CustomDialogModal';

// material-ui
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import AssignmentIcon from '@material-ui/icons/Assignment';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import SettingsIcon from '@material-ui/icons/Settings';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';


const useStyles = makeStyles({
    root: {
        width: 'relative',
    },
});

const Nav = () => {

    // extraer de activity context
    const activityContext = useContext(ActivityContext);
    const { activity } = activityContext;

    // extraer de task context
    const taskContext = useContext(TaskContext);
    const { showForm } = taskContext;

    // extraer de auth context
    const authContext = useContext(AuthContext);
    const { user } = authContext;

    // material-ui
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    // custom material dialog modal
    const [open, setOpen] = React.useState(false);
    const [action, setAction] = React.useState('');
    const handleClickOpen = (action) => {
        setOpen(true);
        setAction(action);
    };
    const handleClose = () => {
        setOpen(false);
    };


    return (
        <>
            <BottomNavigation
                value={value}
                onChange={(event, newValue) => {
                    setValue(newValue);
                }}
                showLabels
                className={classes.root}
            >
                <BottomNavigationAction
                    label="Nueva tarea"
                    onClick={() => showForm()}
                    icon={<AssignmentIcon />}
                />
                {user && user._id === activity.owner ? <div>
                    <BottomNavigationAction
                        // eslint-disable-next-line
                        label="Agregar usuario"
                        onClick={() => handleClickOpen('add_user')}
                        icon={<PersonAddIcon />}
                    />
                    <BottomNavigationAction
                        label="Configurar"
                        icon={<SettingsIcon />}
                        onClick={() => handleClickOpen('update')}
                    />
                    <BottomNavigationAction
                        label="Eliminar"
                        icon={<DeleteForeverIcon />}
                        onClick={() => handleClickOpen('delete')}
                    />
                </div>
                    :
                    <BottomNavigationAction
                        label="Abandonar"
                        icon={<ExitToAppIcon />}
                    />
                }
            </BottomNavigation>

            <CustomDialogModal
                open={open}
                setOpen={setOpen}
                handleClose={handleClose}
                action={action}
                setAction={setAction}
            />
        </>
    );
}

export default Nav;

import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from '../../context/auth/authContext';

// Estilos
import {TextField, makeStyles} from '@material-ui/core';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    textField: {
        margin: theme.spacing(1),
        width: '30ch',
    },
    button: {
        margin: theme.spacing(1),
        width: '30ch',
        
    },
}));



const Signup = (props) => {

    // extraer de authcontext
    const authContext = useContext(AuthContext);
    const { message, authenticated, userRegister } = authContext;

    // en caso que usuario se haya autenticado, registrado o sea un registro duplicado
    useEffect(() => {
        if(authenticated){
            props.history.push('/panel');
        }

        if(message){
            alert('error');
            // showAlert(message.msg, message.category);
        }
    }, [message, authenticated, props.history])

    const [ user, setUser ] = useState({
        userName: '',
        email: '',
        password: '',
        password2: '',        
    });

    // extraer variables de user state
    const { userName, email, password, password2 } = user;

    // funcion para asignar valores a user state
    const onchange = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value,
        });
    }

    // cuando usuario se quiere registrar
    const onsubmit = (e) => {
        e.preventDefault();

        // validar campos vacios
        if(userName.trim() === '' || email.trim() === '' || password === '' || password2 === ''){
            alert("Todos los campos deben ser llenados");
            return;
        }

        // validar que password tenga minimo 6 caracteres
        if(password.length < 6){
            alert("La contraseña debe ser minimo de 6 caracteres");
            return;
        }

        // validar coincidencia de password
        if(password !== password2){
            alert("Las contraseñas no coinciden")
            return;
        }

        // pasar los valores del state actual al context
        userRegister({userName, email, password});
    }

    // inicializar los estilos con material-ui
    const classes = useStyles();

    return <>

        <div className="div-auth">
            <form
                onSubmit={onsubmit}
            >
                <h2 className="text-center">Crear cuenta</h2>

                <div className="">
                    
                    <TextField 
                        type="text" 
                        label="Nombre"
                        fullWidth
                        variant="outlined"
                        className={classes.textField} 
                        id="InputName" 
                        name="userName"
                        onChange={onchange}
                        value={userName}
                    />
                </div>
                <div className="form-group">
                    <TextField 
                        type="email" 
                        label="Email"
                        fullWidth
                        variant="outlined"
                        className={classes.textField} 
                        id="InputEmail" 
                        name="email"
                        onChange={onchange}
                        value={email}
                    />
                </div>
                <div className="">
                    <TextField 
                        type="password" 
                        label="Contraseña"
                        fullWidth
                        variant="outlined"
                        className={classes.textField} 
                        id="InputPassword" 
                        name="password"
                        onChange={onchange}
                        value={password}
                    />
                </div>
                <div className="">
                    <TextField 
                        type="password" 
                        label="Confirmar contraseña"
                        fullWidth
                        variant="outlined"
                        className={classes.textField} 
                        id="InputPassword2" 
                        name="password2"
                        onChange={onchange}
                        value={password2}
                    />
                </div>
                <Button 
                    type="submit" 
                    variant="contained"
                    color="primary"
                    className={classes.button} 
                >Registrarme</Button>
            </form>
            
            <Link to='/' className="link-default">
                Ya tengo una cuenta
            </Link>
        </div>
    </>
}

export default Signup;
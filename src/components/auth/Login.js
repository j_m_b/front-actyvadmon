import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from '../../context/auth/authContext';

// Estilos
import {TextField, makeStyles} from '@material-ui/core';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    textField: {
        margin: theme.spacing(1),
        width: '30ch',
    },
    button: {
        margin: theme.spacing(1),
        width: '30ch',
        
    },
}));




const Login = (props) => {

    // extraer de auth context
    const authContext = useContext(AuthContext);
    const { message, authenticated, logIn } = authContext;

    // en caso que el password o el usuario no exista
    useEffect(() => {
        if(authenticated){
            props.history.push('/panel');
        }

        if(message){
            alert(message.msg);
        }
    }, [message, authenticated, props.history])

    // state para iniciar sesion
    const [ user, setUser ] = useState({
        email: '',
        password: '',
    })

    // extraer del state user
    const { email, password } = user;

    // actualizo el state
    const onchange = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value,
        })
    }

    // cuando usuario quiere iniciar sesion
    const onsubmit = (e) => {
        e.preventDefault();

        // validar 
        if (email.trim() === '' || password.trim() === '') {
            alert('Campos obligatorios');
            return;
        }

        // si pasa validacion
        logIn({ email, password })
    }

    // inicializar los estilos con material-ui
    const classes = useStyles();

    return <>
        <div className="div-auth">
            <h2 className="text-center">Iniciar sesión</h2>
            <form
                onSubmit={onsubmit}
            >
                <div className="">
                    <TextField 
                        type="email" 
                        name="email"
                        label="Email"
                        fullWidth
                        variant="outlined"
                        className={classes.textField} 
                        onChange={onchange}
                        value={email}
                        color="primary"
                        id="InputEmail" 
                    />
                </div>
                <div className="">
                    <TextField 
                        type="password" 
                        name="password"
                        label="Contraseña"
                        fullWidth
                        variant="outlined"
                        className={classes.textField} 
                        onChange={onchange}
                        value={password}
                        color="primary"
                        id="InputPassword" 
                    />
                </div>
                <Button 
                    type="submit" 
                    variant="contained"
                    color="primary"
                    className={classes.button} 
                >Ingresar</Button>
            </form>

            <Link to='/nueva-cuenta' className="link-default">
                Crear cuenta
            </Link>
        </div>
    </>
}

export default Login;
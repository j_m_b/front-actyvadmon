// este Order Higher Component se usa mas que nada para proteger los componentes

import React, { useContext, useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthContext from '../../context/auth/authContext';


const PrivateRoute = ({ component: Component, ...props }) => {

    // extraer del context de auth
    const authContext = useContext(AuthContext);
    const { authenticated, loading, userAuthenticate } = authContext;

    // verificar si hay un usuario autenticado
    useEffect(() => {
        userAuthenticate();
        // eslint-disable-next-line
    }, []);

    return (
        <Route {...props} render={ props => !authenticated && !loading ? (
                <Redirect to="/" />
            ) : (
                <Component {...props} />
            )} 
        />
    );
}

export default PrivateRoute;
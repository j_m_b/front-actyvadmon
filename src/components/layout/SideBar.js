import React, { useContext, useEffect } from 'react';
import ActivityContext from '../../context/activities/activityContext';
import AuthContext from '../../context/auth/authContext';
import NewActivity from '../activities/NewActivity';
import ListOfActivities from '../activities/ListOfActivities';
// import PropTypes from 'prop-types';


// Estilos Material-ui
import {
    Button,
    Avatar,
    AppBar,
    CssBaseline,
    Divider,
    Drawer,
    Hidden,
    IconButton,
    Toolbar,
    Typography,
} from '@material-ui/core/';


import MenuIcon from '@material-ui/icons/Menu';

import { makeStyles, useTheme } from '@material-ui/core/styles';


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHead: {
        margin: '1rem',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));



const SideBar = (props) => {

    // extraer de activity context
    const activityContext = useContext(ActivityContext);
    const { activity } = activityContext;

    // extraer de auth context
    const authContext = useContext(AuthContext);
    const { user, userAuthenticate, logOut } = authContext;

    useEffect(() => {
        userAuthenticate();
        // eslint-disable-next-line
    }, []);

    // Material-UI //
    const { window } = props;
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);


    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    // Drawer o sidebar para material-ui
    const drawer = (
        <div>
            <div className={classes.toolbar} />
            <div className={classes.drawerHead}>
                <Avatar variant="square" className={classes.square}>
                    { user ? user.userName[0] : null }
                </Avatar>
                <Typography variant="h5" noWrap align="center">
                    { user ? user.userName : null }
                </Typography>
                <Typography paragraph align="center">
                    { user ? user.email : null }
                </Typography>
                <Button
                    color="secondary"
                    fullWidth
                    onClick={() => logOut()}
                >Cerrar sesión</Button>
            </div>
            <Divider />
            <NewActivity />
            <Divider />
            <ListOfActivities />
            <Divider />
        </div>
    );

    const container = window !== undefined ? () => window().document.body : undefined;


    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap>
                        { activity ? activity.name : 'ActyvAdmon'}
                    </Typography>
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer} aria-label="mailbox folders">
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Hidden smUp implementation="css">
                    <Drawer
                        container={container}
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
        </div>
    );
}

export default SideBar;

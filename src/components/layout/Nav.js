import React, { useContext, useEffect } from 'react';
import AuthContext from '../../context/auth/authContext';
import styled from '@emotion/styled'



const NavHeader = styled.div`
    
    background-color: #fff;
    padding: 0.5rem;
    text-align: center;

    p{
        
        color: #000;
        font-size: 2.2rem;
        margin: 0;

        span {
            font-weight: 900;
        }
    }
`;


const Nav = () => {

    // extraer de authcontext
    const authContext = useContext(AuthContext);
    const { user, userAuthenticate } = authContext;

    useEffect(() => {
        userAuthenticate();
        // eslint-disable-next-line
    }, []);

    return <>
        <header>
            <NavHeader>
                { user ? (
                    <p>Hola <span>{ user.userName }</span></p>
                ) : (
                    <p><span>ActyvAdmon</span></p>
                )}
            </NavHeader>
        </header>
    </>
}

export default Nav;

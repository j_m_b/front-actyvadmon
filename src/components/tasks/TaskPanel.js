import React, {useContext} from 'react';
import MemberList from '../activities/MembersList';
import TasksList from './TasksList';
import TaskForm from './TaskForm';
import TaskContext from '../../context/tasks/taskContext';



// material-ui
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
    
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

const TaskPanel = () => {

    // extraer de task context
    const taskContext = useContext(TaskContext);
    const { taskForm } = taskContext;

    // material-ui
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Tabs value={value} onChange={handleChange} aria-label="simple tabs example" variant="fullWidth">
                    <Tab label="Tareas" {...a11yProps(0)} />
                    <Tab label="Miembros" {...a11yProps(1)} />
                </Tabs>
            </AppBar>
            { taskForm ? (
                <TaskForm />
            ): null}
            <TabPanel value={value} index={0}>
                <TasksList />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <MemberList />
            </TabPanel>
        </div>
    );
}

export default TaskPanel;
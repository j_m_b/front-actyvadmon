import React, { useContext, useState, useEffect } from 'react';
import TaskContext from '../../context/tasks/taskContext';
import ActivityContext from '../../context/activities/activityContext';

// material-ui
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import DoneIcon from '@material-ui/icons/Done';
import CancelIcon from '@material-ui/icons/Cancel';


const useStyles = makeStyles((theme) => ({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 'relative',
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
}));

const TaskUpdateForm = () => {

    // extraer de task context
    const taskContext = useContext(TaskContext);
    const { taskSelected, taskList, updateTask, cleanTask } = taskContext;

    // extraer de activity context
    const activityContext = useContext(ActivityContext);
    const { activity } = activityContext;

    // state local para guardar nueva tarea
    const [task, setTask] = useState({
        name: '',
    })

    // effect detecta si hay una tarea seleccionada
    useEffect( () => {
        if (taskSelected !== null) {
            setTask(taskSelected)
        } else {
            setTask({
                name: '',
            })
        }
    }, [taskSelected])

    // extraer de task state
    const { name } = task;

    const handleChange = (e) => {
        setTask({
            ...task,
            [e.target.name]: e.target.value
        })
    }

    // guardar tarea
    const onsubmitTask = (e) => {
        e.preventDefault();

        // validar campos vacios
        if (name.trim() === '') {
            alert("Debe llenar todos los campos");
            return;
        }

        // actualizo el task seleccionado
        updateTask(task);

        // limpiar formulario
        setTask({
            name: ''
        })

        // obtener lista de tareas
        const currentActivityID = activity._id;
        taskList(currentActivityID._id);

        // esconder el formulario
        cleanTask();
    }

    // material-ui
    const classes = useStyles();

    return (
        <Paper
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={onsubmitTask}
        >
            <InputBase
                type="text"
                name="name"
                onChange={handleChange}
                value={name}
                className={classes.input}
                placeholder="Tarea, ex: Contactar clientes"
                inputProps={{ 'aria-label': 'Tarea, ex: Contactar clientes' }}
            />
            <IconButton
                type="submit"
                className={classes.iconButton} 
                aria-label="Guardar"
                color="primary"
            >
                <DoneIcon />
            </IconButton>
            <IconButton
                type="button"
                onClick={() => cleanTask()}
                className={classes.iconButton} 
                aria-label="Cancelar"
                color="secondary"
            >
                <CancelIcon />
            </IconButton>
        </Paper>
    );
}

export default TaskUpdateForm;
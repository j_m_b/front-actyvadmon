import React, { useContext } from 'react';
import TaskContext from '../../context/tasks/taskContext';
import ActivityContext from '../../context/activities/activityContext';
import TaskUpdateForm from './TaskUpdateForm';


// material-ui
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 'relative',
        backgroundColor: theme.palette.background.paper,
    },
}));


const TasksList = () => {

    // extraer de task context
    const taskContext = useContext(TaskContext);
    const { tasks, taskSelected, currentTask, taskList, updateTask, deleteTask, cleanTask } = taskContext;

    // extraer de activity context
    const activityContext = useContext(ActivityContext);
    const { activity } = activityContext;

    // eliminar tarea y actualizar state
    const removeTask = (taskID) => {
        deleteTask(taskID, activity._id);
        taskList(activity._id);
    }

    // editar estado 
    const handleChange = (task) => () => {
        let newStatus = null;

        if (task.status) {
            newStatus = false;
        } else {
            newStatus = true;
        }


        task.status = newStatus;
        updateTask(task);
        task = null;
    };

    // activar form para edicion
    const editTask = (task) => {
        cleanTask();
        currentTask(task);
    }


    // material-ui
    const classes = useStyles();



    return <>
        {tasks.length !== 0 ? (
            <List subheader={<ListSubheader>Lista de tareas</ListSubheader>} className={classes.root}>
                {tasks.map(task => (
                    <ListItem key={task._id}>
                        {taskSelected && task._id === taskSelected._id ?
                            <TaskUpdateForm />
                            : <>
                                <Checkbox
                                    checked={task.status === true}
                                    color="primary"
                                    onChange={handleChange(task)}
                                />
                                <ListItemText id={`switch-list-label-${task.name}`} primary={task.name} />
                                <ListItemSecondaryAction>
                                    <IconButton
                                        edge="end"
                                        type="button"
                                        onClick={() => editTask(task)}
                                        className={classes.iconButton}
                                        aria-label="Editar"
                                        color="primary"
                                    >
                                        <EditIcon />
                                    </IconButton>
                                    <IconButton
                                        edge="end"
                                        type="button"
                                        onClick={() => removeTask(task._id)}
                                        className={classes.iconButton}
                                        aria-label="Eliminar"
                                        color="secondary"
                                    >
                                        <DeleteForeverIcon />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </>}
                    </ListItem>
                ))}
            </List>
        ) : (<Typography
            component="span"
            variant="body2">
                No hay tareas registradas para esta actividad
            </Typography>)}
    </>
}

export default TasksList;
import axios from 'axios';

// crear el axiosCliente para comunicacion restapi
console.log(process.env.REACT_APP_BACKEND_URL);
const axiosClient = axios.create({
    baseURL: process.env.REACT_APP_BACKEND_URL,
});

export default axiosClient;
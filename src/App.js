import React from 'react';
import './App.css';
// componentes
import Nav from './components/layout/Nav'
import Login from './components/auth/Login';
import Signup from './components/auth/Signup';
import Activities from './components/activities/Activities';

// contextos
import AuthState from './context/auth/authState';
import ActivityState from './context/activities/activityState';
import TaskState from './context/tasks/taskState';

// rutas privadas -> importar order higher component -> toma un componente dentro de el y evalua
import PrivateRoute from './components/private_routes/PrivateRoute';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import tokenAuth from './config/tokenAuth';

// revisar si tenemos un token
const token = localStorage.getItem('token');
if (token) {
    tokenAuth(token);
}

function App() {
    return <>
        <AuthState>
            <Nav />
            <ActivityState>
                <TaskState>
                    <Router>
                        <Switch>
                            <Route exact path="/" component={Login} />
                            <Route exact path="/nueva-cuenta" component={Signup} />
                            <PrivateRoute exact path="/panel" component={Activities} />
                        </Switch>
                    </Router>
                </TaskState>
            </ActivityState>
        </AuthState>
    </>
}

export default App;

import { 
    TASK_FORM,
    TASK_FORM_DISABLE,
    TASKS_LIST,
    NEW_TASK,
    TASK_FORM_VALIDATION,
    CURRENT_TASK,
    UPDATE_TASK,
    REMOVE_TASK,
    CLEAN_CURRENT_TASK,
} from '../../types';

export default (state, action) => {
    switch (action.type) {
        case TASK_FORM:
            return{
                ...state,
                taskForm: true,
            }
        case TASK_FORM_DISABLE:
            return{
                ...state,
                taskForm: false,
            }
        case TASKS_LIST:
            return{
                ...state,
                tasks: action.payload,
            }
        case NEW_TASK:
            return{
                ...state,
                tasks: [action.payload, ...state.tasks],
                taskForm: false,
                taskError: false,
            }
        case TASK_FORM_VALIDATION:
            return{
                ...state,
                taskError: true,
            }
        case CURRENT_TASK:
            return{
                ...state,
                taskSelected: action.payload
                // task: state.tasks.filter(task => task._id === action.payload)
            }
        case UPDATE_TASK:
            return{
                ...state,
                tasks: state.tasks.map(task => task._id === action.payload._id ? action.payload : task),

            }
        case REMOVE_TASK:
            return{
                ...state,
                tasks: state.tasks.filter(task => task._id !== action.payload),
                taskSelected: null,
            }
        case CLEAN_CURRENT_TASK:
            return{
                ...state,
                taskSelected: null
            }
            
        default:
            return state;
    }
}
import React, {useReducer} from 'react';
import taskContext from './taskContext';
import taskReducer from './taskReducer';

import { 
    TASK_FORM,
    TASK_FORM_DISABLE,
    TASKS_LIST,
    NEW_TASK,
    TASK_FORM_VALIDATION,
    CURRENT_TASK,
    CLEAN_CURRENT_TASK,
    UPDATE_TASK,
    REMOVE_TASK,
} from '../../types';

import axiosClient from '../../config/axios';


const TaskState = (props) => {

    // state inicial para tareas
    const initialState = {
        tasks: [],
        taskSelected: null,
        taskForm: false,
        taskError: false,
    }

    // crear dispatch para ejecutar acciones en el state
    const [state, dispatch] = useReducer(taskReducer, initialState);

    // FUNCIONES PARA EJECUTAR CRUD //

    // mostrar formulario para nueva tarea
    const showForm = () => {
        dispatch({
            type: TASK_FORM
        })
    }

    // ocultar formulario para nueva tarea
    const hideForm = () => {
        dispatch({
            type: TASK_FORM_DISABLE
        })
    }

    // obtener lista de tareas de la actividad
    const taskList = async (activity) => {
        try {
            const response = await axiosClient.get('/api/tasks/', { params: {activity} });
            dispatch({
                type: TASKS_LIST,
                payload: response.data
            })
        } catch (error) {
            console.log(error);
        }
    }

    // si hay error en formulario
    const showError = () => {
        dispatch({
            type: TASK_FORM_VALIDATION
        })
    }

    // crear nueva tarea
    const newTask = async (task) => {
        try {
            const response = await axiosClient.post('/api/tasks/', task);
            dispatch({
                type: NEW_TASK,
                payload: response.data
            })
        } catch (error) {
            console.log(error);
        }
    }

    // seleccionar tarea para ponerla en el state de task
    const currentTask = (task) => {
        dispatch({
            type: CURRENT_TASK,
            payload: task
        })
    }

    // actualizar tarea
    const updateTask = async (task) => {
        try {
            const response = await axiosClient.put(`/api/tasks/${task._id}`, task);
            dispatch({
                type: UPDATE_TASK,
                payload: response.data
            })
        } catch (error) {
            console.log(error)
        }
    }

    // eliminar tarea
    const deleteTask = async (taskID, activity) => {
        try {
            const response = await axiosClient.delete(`/api/tasks/${taskID}`, { params: {activity} });
            console.log(response.data);
            dispatch({
                type: REMOVE_TASK,
                payload: taskID
            })
        } catch (error) {
            console.log(error);
        }
    }

    // limpio el state de task
    const cleanTask = () => {
        dispatch({
            type: CLEAN_CURRENT_TASK
        })
    }

    return <>
        <taskContext.Provider
            value={{
                tasks: state.tasks,
                taskSelected: state.taskSelected,
                taskForm: state.taskForm,
                taskError: state.taskError,
                showForm,
                hideForm,
                taskList,
                showError,
                newTask,
                currentTask,
                updateTask,
                deleteTask,
                cleanTask,
            }}
        >
            { props.children }
        </taskContext.Provider>
    </>
}

export default TaskState;
import {
    ACTIVITY_FORM,
    ACTIVITY_FORM_DISABLE,
    ACTIVITIES_LIST,
    NEW_ACTIVITY,
    ACTIVITY_FORM_VALIDATION,
    CURRENT_ACTIVITY,
    REMOVE_ACTIVITY,
    UPDATE_ACTIVITY,
    ACTIVITY_ERROR,
    MEMBERS_LIST,
    REMOVE_MEMBER,
    ADD_MEMBER
} from '../../types';

export default (state, action) => {
    switch (action.type) {
        case ACTIVITY_FORM:
            return {
                ...state,
                activityForm: true,
            };
        case ACTIVITY_FORM_DISABLE:
            return {
                ...state,
                activityForm: false,
            }
        case ACTIVITIES_LIST:
            return {
                ...state,
                activities: action.payload
            }
        case NEW_ACTIVITY:
            return {
                ...state,
                activities: [...state.activities, action.payload],
                activityForm: false,
                activityError: false
            }
        case ACTIVITY_FORM_VALIDATION:
            return {
                ...state,
                activityError: true
            }
        case CURRENT_ACTIVITY:
            return {
                ...state,
                activity: action.payload
                // activity: state.activities.filter(activity => activity._id === action.payload)
            }
        case UPDATE_ACTIVITY:
            return {
                ...state,
                activities: state.activities.map(activity => activity._id === action.payload._id ? action.payload : activity),
            }
        case REMOVE_ACTIVITY:
            return {
                ...state,
                activities: state.activities.filter(activity => activity._id !== action.payload),
                activity: null
            }
        case ACTIVITY_ERROR:
            return {
                ...state,
                message: action.payload
            }
        case MEMBERS_LIST:
            return{
                ...state,
                members: action.payload
            }
        case REMOVE_MEMBER:
            return{
                ...state,
                members: state.members.filter(member => member !== action.payload)
            }
        case ADD_MEMBER:
            return{
                ...state,
                members: [...state.members, action.payload]
            }
        default:
            return state;
    }
}
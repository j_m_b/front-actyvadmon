import React, { useReducer } from 'react';

import activityContext from './activityContext';
import activityReducer from './activityReducer';

import {
    ACTIVITY_FORM,
    ACTIVITY_FORM_DISABLE,
    ACTIVITIES_LIST,
    NEW_ACTIVITY,
    ACTIVITY_FORM_VALIDATION,
    CURRENT_ACTIVITY,
    UPDATE_ACTIVITY,
    REMOVE_ACTIVITY,
    ACTIVITY_ERROR,
    MEMBERS_LIST,
    REMOVE_MEMBER,
    ADD_MEMBER
} from '../../types';

import axiosClient from '../../config/axios';


const ActivityState = (props) => {

    const initialState = {
        activities: [],
        activity: null,
        activityForm: false,
        activityError: false,
        message: null,
        members: [],
    }

    // dispatch para ejecutar acciones en el state
    const [ state, dispatch ] = useReducer(activityReducer, initialState);

    
    // SERIE DE FUNCIONES PARA EL CRUD //

    // alertas en casos de error de un success
    const alert = {
        msg: 'Error',
        category: 'error-alert'
    }

    // activar formulario para agregar nueva actividad
    const showForm = () => {
        dispatch({
            type: ACTIVITY_FORM
        })
    }

    // desactiva formulario para agregar nueva actividad
    const hideForm = () => {
        dispatch({
            type: ACTIVITY_FORM_DISABLE
        })
    }

    // obtener las actividades
    const activitiesList = async () => {
        try {
            const response = await axiosClient.get('/api/activities');
            dispatch({
                type: ACTIVITIES_LIST,
                payload: response.data
            })
        } catch (error) {
            dispatch({
                type: ACTIVITY_ERROR,
                payload: alert
            })
        }
    }

    // validar formulario por error
    const showError = () => {
        dispatch({
            type: ACTIVITY_FORM_VALIDATION
        })
    }

    // nueva actividad
    const newActivity = async (activity) => {
        try {
            const response = await axiosClient.post('/api/activities', activity);
            dispatch({
                type: NEW_ACTIVITY,
                payload: response.data
            })
        } catch (error) {
            dispatch({
                type: ACTIVITY_ERROR,
                payload: alert
            })
        }
    }

    // seleccionar una sola actividad
    const currentActivity = async (activityID) => {
        try {
            const response = await axiosClient.get(`/api/activities/${activityID}`);
            dispatch({
                type: CURRENT_ACTIVITY,
                payload: response.data[0]
            })
        } catch (error) {
            dispatch({
                type: ACTIVITY_ERROR,
                payload: alert
            })
        }
        
    }

    // actualizar una actividad
    const updateActivity = async (activity) => {
        try {
            const response = await axiosClient.put(`/api/activities/${activity._id}`, activity);
            dispatch({
                type: UPDATE_ACTIVITY,
                payload: response.data.activity,
            })
            
            if (activity.email && !activity.actionDelete) {
                addMember(response.data.activity.members[response.data.activity.members.length - 1]);
            }
        } catch (error) {
            dispatch({
                type: ACTIVITY_ERROR,
                payload: alert,
            })
        }
    }

    // eliminar una actividad
    const removeActivity = async (activityID) => {
        try {
            await axiosClient.delete(`/api/activities/${activityID}`);
            dispatch({
                type: REMOVE_ACTIVITY,
                payload: activityID
            })
        } catch (error) {
            dispatch({
                type: ACTIVITY_ERROR,
                payload: alert
            })
        }
    }

    // lista de miembros de actividad
    const memberList = (members) => {
        try {
            dispatch({
                type: MEMBERS_LIST,
                payload: members
            })
        } catch (error) {
            dispatch({
                type: ACTIVITY_ERROR,
                payload: alert
            })
        }
    }

    // agregar miembro al state de members
    const addMember = (user) => {
        try {
            dispatch({
                type: ADD_MEMBER,
                payload: user,
            })
        } catch (error) {
            dispatch({
                type: ACTIVITY_ERROR,
                payload: alert
            })
        }
    }

    // eliminar miembro del state de members
    const removeMember = (user) => {
        try {
            dispatch({
                type: REMOVE_MEMBER,
                payload: user
            })
        } catch (error) {
            dispatch({
                type: ACTIVITY_ERROR,
                payload: alert
            })
        }
    }


    // retornar contexto
    return (
        <activityContext.Provider
            value={{
                activities: state.activities,
                activity: state.activity,
                activityForm: state.activityForm,
                activityError: state.activityError,
                message: state.message,
                members: state.members,
                showForm,
                hideForm,
                activitiesList,
                showError,
                newActivity,
                currentActivity,
                updateActivity,
                removeActivity,
                memberList,
                removeMember,
                addMember
            }}
        >
            { props.children }
        </activityContext.Provider>
    ) 
}

export default ActivityState;
import React, { useReducer } from 'react';
import authReducer from './authReducer';
import authContext from './authContext';
import axiosClient from '../../config/axios';
import tokenAuth from '../../config/tokenAuth';

import {
    REGISTER_SUCCESS,
    REGISTER_ERROR,
    CURRENT_USER,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    LOG_OUT,
} from '../../types';


const AuthState = (props) => {
    
    // initiaState
    // user <- objeto usuario
    // loading <- para evitar efecto de ver login form cuando se esta authenticated en el portal principal
    const initialState = {
        token: localStorage.getItem('token'),
        authenticated: null,
        user: null,
        message: null,
        loading: true,
    }

    const [ state, dispatch ] = useReducer(authReducer, initialState);


    // registrar nuevo usuario
    const userRegister = async (data) => {
        try {
            const response = await axiosClient.post('/api/user', data);

            dispatch({
                type: REGISTER_SUCCESS,
                payload: response.data
            })
            userAuthenticate();
        } catch (error) {
            // category es un elemento de clase para bootstrap
            console.log(error);
            const alert = {
                msg: error.response.data.msg,
                category: 'error'
            }

            dispatch({
                type: REGISTER_ERROR,
                payload: alert,
            })
        }
    }


    // cuando usuario se registar o inicia sesion <- activa states globales de login
    const userAuthenticate = async () => {
        // tomar el token de local storage del navegador
        const token = localStorage.getItem('token');
        if (token) {
            // crear en el header la variable x-auth-token con este valor 
            tokenAuth(token);
        }

        try {
            // respuesta de back
            const response = await axiosClient.get('/api/auth');
            dispatch({
                type: CURRENT_USER,
                payload: response.data.user
            })
        } catch (error) {
            // console.log(error);
            dispatch({
                type: LOGIN_ERROR,
            })
        }
    }


    // cuando usuario inicia sesion
    const logIn = async (data) => {
        try {
            const response = await axiosClient.post('/api/auth', data);
            // console.log(response);
            dispatch({
                type: LOGIN_SUCCESS,
                payload: response.data
            })
            userAuthenticate();
        } catch (error) {
            console.log(error);
            const alert = {
                msg: error.response.data.msg,
                category: 'error',
            }

            dispatch({
                type: LOGIN_ERROR,
                payload: alert,
            })
        }
    }


    // cuando usuario quiere cerrar sesion
    const logOut = () => {
        dispatch({
            type: LOG_OUT
        })
    }


    return <authContext.Provider
            value={{
                token: state.token,
                authenticated: state.authenticated,
                user: state.user,
                message: state.message,
                loading: state.loading,
                userRegister,
                userAuthenticate,
                logIn,
                logOut
            }}
        >
        {props.children}
    </authContext.Provider>
}


export default AuthState;